package id.ac.ui.cs.tutorial0.service;

public class powerClassifier {
    public String classify(int power) {
        if(power>100000){
            return "A Class";
        }
        else if(power>20000){
            return  "B Class";
        }
        else {
            return "C Class";
        }
    }
}
