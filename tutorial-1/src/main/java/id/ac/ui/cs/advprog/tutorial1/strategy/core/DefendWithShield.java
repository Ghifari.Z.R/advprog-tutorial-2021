package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "Shield on";
    }

    @Override
    public String getType() {
        return "DefendWithShield";
    }

}
