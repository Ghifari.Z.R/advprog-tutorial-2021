package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild=guild;
    }

    @Override
    public void update() {
        String questtype = this.guild.getQuestType();
        if(questtype.equals("D")||questtype.equals("E")){
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
