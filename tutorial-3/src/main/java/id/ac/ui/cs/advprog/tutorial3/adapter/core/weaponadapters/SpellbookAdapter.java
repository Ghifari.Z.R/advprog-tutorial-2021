package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean islargespell;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook=spellbook;
        this.islargespell=false;
    }

    @Override
    public String normalAttack() {
        this.islargespell=false;
        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (this.islargespell==false){
            this.islargespell=true;
            return this.spellbook.largeSpell();
        }
        else {
            this.islargespell=false;
            return  "cannot using large spell";
        }
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

}
