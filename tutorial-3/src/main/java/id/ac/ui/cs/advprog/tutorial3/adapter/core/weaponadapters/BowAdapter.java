package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean isaim;
    public BowAdapter(Bow bow){
        this.bow=bow;
        this.isaim=false;
    }
    @Override
    public String normalAttack() {

        return this.bow.shootArrow(isaim);
    }

    @Override
    public String chargedAttack() {
        this.isaim= !this.isaim;
        if(this.isaim){
            return "entering aimshot mode";
        }
        else{
            return "leaving aimshot mode";
        }
    }

    @Override
    public String getName() {

        return this.bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return this.bow.getHolderName();
    }
}
